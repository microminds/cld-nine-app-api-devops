terraform {
  backend "s3" {
    bucket         = "cld-nine-app-api-devops-tfstate"
    key            = "cld-nine-app.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "cld-nine-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}